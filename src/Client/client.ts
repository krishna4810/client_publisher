import net from 'net';
import { IClient } from '../interfaces/iclient';

export class IMQClient implements IClient {
    private clientSocket: net.Socket;
    constructor(serverIp: string, serverPort: number) {
        this.clientSocket = net.connect({ port: 3050 }, () => {
            console.log('connected to server!');
            // this.sendMessage('Hello, server.');
        });
        this.setupClientListeners();
    }

    public sendMessage(message: string) {
        this.clientSocket.write(message);
    }

    public close() {
        this.clientSocket.end();
    }

    private setupClientListeners() {
        this.clientSocket.on('data', function (data: any) {
            console.log(data.toString());
        });

        this.clientSocket.on('end', function () {
            console.log('disconnected from server');
        });
    }

}