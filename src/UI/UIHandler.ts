import { EventEmitter } from "events";
import { Pages } from "../enums/pages";
import { CONSOLE } from "./console";

export class UIHandler {
    private cli: CONSOLE;
    private UIInputs: EventEmitter;
    private currentPage: Pages;
    private currentUserEmail = null;
    constructor() {
        this.cli = new CONSOLE();
        this.UIInputs = this.cli.getInputEventEmitter();
        this.currentPage = Pages.WELCOME;
    }

    registerForInputEvents() {
        this.UIInputs.on("input", (input: any) => {
            switch (this.currentPage) {
                case Pages.WELCOME:
                    break;
                case Pages.LOGIN:
                    break;
                case Pages.REGISTER:
                    break;
                case Pages.MAIN:
                    break;
                case Pages.PUBLISH:
                    break;
            }

        })
    }
}