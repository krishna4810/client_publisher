import { IMQClient } from "../Client/client";
import { IClient } from "../interfaces/iclient";


import readline from "readline";
import { EventEmitter } from "events";


export class CONSOLE {
    private consoleUI: readline.Interface;
    eventEmitter = new EventEmitter();
    constructor() {
        this.consoleUI = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }
    write(message: string) {
        console.log(message);
    }
    getInputEventEmitter() {
        this.consoleUI.on('line', (message) => {
            this.eventEmitter.emit("input", message);
        });

        this.consoleUI.on("close", () => {
            this.eventEmitter.emit("closed");
        });

        return this.eventEmitter;
        
    }

}