import { IMQClient } from "./Client/client";
import config from "./configuration/config.json"
import { IClient } from "./interfaces/iclient";
import { CONSOLE } from "./UI/console";

let app: IClient;
let ui;
function startApp() {
    app = new IMQClient(config.imqHost, config.imqPort);
    ui = new CONSOLE(app);
    ui.start()
}

startApp();
