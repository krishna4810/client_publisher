export enum Pages{
    LOGIN = 'login',
    MAIN = 'main',
    PUBLISH = 'publish',
    REGISTER = 'register',
    WELCOME = 'welcome'
}