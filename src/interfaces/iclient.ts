export interface IClient{
    sendMessage(message: string): void;
    close(): void;
}